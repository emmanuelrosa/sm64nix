{ lib
, stdenv
, symlinkJoin
, glew
, SDL2
, python3
, fetchFromGitHub
, gnused
, util-linux
, makeDesktopItem
, copyDesktopItems
, ensureNewerSourcesForZipFilesHook
, writeScript
, bash
, sm64coopdx-unwrapped
}: 
{ name ? "sm64coopdx"     # Name for your package; Must not have spaces. Will also be the name of the executable. 
                          # You can install multiple packages with different names, but don't *run* them at the same time.
, rom                     # Path to the Mario 64 US ROM. Must be commited to your Nix flake repo.
, modelPacks ? [ ]        # List of DynOS Nix packages.
, mods ? [ ]              # List of sm64coopdx (Lua) mod Nix packages.
, enableWayland ? false   # When set to 'true', Wayland is used for rendering, if it's available.
}:
let
  pname = name;
  unwrapped = sm64coopdx-unwrapped.override { inherit rom; };

  mergedModelPacks = symlinkJoin {
    name = "${pname}-modelpacks";
    paths = modelPacks;
  };

  mergedMods = symlinkJoin {
    name = "${pname}-mods";
    paths = mods;
  };

  launcher = writeScript pname ''
    #! ${bash}/bin/bash
    # The following comments reference the paths to the model and texture packages.
    # This is so that the Nix garbage collector doesn't delete the mod packages.
    # Some of them can be rather large, so it makes sense to keep them as long as
    # this package is installed.
    ${lib.optionalString (modelPacks != []) "# ${mergedModelPacks}"}
    ${lib.optionalString (mods != []) "# ${mergedMods}"}

    ${lib.optionalString enableWayland "if [ -n '$WAYLAND_DISPLAY' ]; then export SDL_VIDEODRIVER=wayland; fi"}
    cd @out@/share/${pname}
    ./sm64coopdx $@
  ''; 
in stdenv.mkDerivation rec {
  inherit pname;
  version = unwrapped.version;
  src = unwrapped;
  nativeBuildInputs = [ copyDesktopItems ];

  buildPhase = ''
    ${lib.optionalString (modelPacks != []) "cp -r ${mergedModelPacks}/. share/${unwrapped.pname}/"}
    ${lib.optionalString (modelPacks != []) "chmod -R u+w share/${unwrapped.pname}/."}
    ${lib.optionalString (mods != []) "cp -r ${mergedMods}/. share/${unwrapped.pname}/mods/"}
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/icons/hicolor/16x16/apps/
    mkdir -p $out/share/${pname}
    mkdir -p $out/bin
    cp -r share/${unwrapped.pname}/. $out/share/${pname}/
    install -D -m 777 ${launcher} $out/bin/${pname}
    substituteAllInPlace $out/bin/${pname}
    ln -s $src/share/icons/hicolor/16x16/apps/${unwrapped.pname}.png $out/share/icons/hicolor/16x16/apps/${pname}.png

    runHook postInstall
  '';

  desktopItems = [
    (makeDesktopItem {
      name = pname;
      exec = pname;
      icon = pname;
      desktopName = "Super Mario 64 (Coop DX)";
      categories = [ "Game" "RolePlaying" ];
      })
  ];

  meta = with lib; {
    description = "Super Mario 64 ported to PC, with updated models and optional HD textures.";
    homepage = "https://github.com/Render96/Render96ex";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}
