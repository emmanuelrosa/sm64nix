{
  description = "Super Mario 64 for NixOS";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/master";
  inputs.erosanix.url = "github:emmanuelrosa/erosanix/master";
  inputs.erosanix.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nix-appimage.url = "github:ralismark/nix-appimage";
  inputs.nix-appimage.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, erosanix, nix-appimage }:
    let
      lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";
      version = builtins.substring 0 8 lastModifiedDate;
      supportedSystems = [ "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
    in

    {
      # Provide Nix functions for selected system types.
      lib = forAllSystems (system:
        {
          mkrender96 = (nixpkgsFor.${system}).callPackage ./lib/mkrender96.nix { render96-unwrapped = self.packages.${system}.render96-unwrapped; };
          mksm64coopdx = (nixpkgsFor.${system}).callPackage ./lib/mksm64coopdx.nix { sm64coopdx-unwrapped = self.packages.${system}.sm64coopdx-unwrapped; };
          nanogl = erosanix.lib.${system}.nanogl;
        });

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        {
          render96-example = (nixpkgsFor.${system}).callPackage ./examples/render96-example.nix {
            inherit (self.lib.${system}) mkrender96;
            sm64pkgs = self.packages.${system};
          };

          sm64coopdx-example = (nixpkgsFor.${system}).callPackage ./examples/sm64coopdx-example.nix {
            inherit (self.lib.${system}) mksm64coopdx;
            sm64pkgs = self.packages.${system};
          };

          render96-nanogl-example = self.lib.${system}.nanogl self.packages.${system}.render96-example [];
          render96-nanogl-example-appimage = nix-appimage.bundlers."${system}".default self.packages."${system}".render96-nanogl-example;
          render96-unwrapped = (nixpkgsFor.${system}).callPackage ./pkgs/variants/render96/default.nix { rom = ./dummyrom.z64; };
          render96-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/models/render96-modelpack/default.nix { };
          render96-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/render96-texturepack/default.nix { };
          sm64coopdx-unwrapped = (nixpkgsFor.${system}).callPackage ./pkgs/variants/sm64coopdx/default.nix { rom = ./dummyrom.z64; };
          sm64coopdx-nanogl-example = self.lib.${system}.nanogl self.packages.${system}.sm64coopdx-example [];
          sm64coopdx-nanogl-example-appimage = nix-appimage.bundlers."${system}".default self.packages."${system}".sm64coopdx-nanogl-example;
          resrgan-16x-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/resrgan-16x-texturepack/default.nix { };
          p3st-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/p3st-texturepack/default.nix { };
          sm64redrawn-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/sm64redrawn-texturepack/default.nix { };
          sm64redrawn-alt-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/sm64redrawn-texturepack/default.nix { useAlternativeTextures = true; };
          super-mario-64-hds-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/super-mario-64-hds/default.nix { };
          sm64reloaded-texturepack = (nixpkgsFor.${system}).callPackage ./pkgs/textures/sm64reloaded/default.nix { };
          javoni-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/models/javoni-modelpack/default.nix { };
          javoni-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/javoni/default.nix { };
          hanyuu-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/models/hanyuu-modelpack/default.nix { };
          analogue-brothers-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/models/analogue-brothers/default.nix { };
          jumpman-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/models/jumpman/default.nix { };
          klonoa-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/klonoa/default.nix { };
          widdle-pets = (nixpkgsFor.${system}).callPackage ./pkgs/mods/widdle-pets/default.nix { };
          sponge-bob-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/sponge-bob/default.nix { };
          the-underworld = (nixpkgsFor.${system}).callPackage ./pkgs/mods/the-underworld/default.nix { };
          geoguessr = (nixpkgsFor.${system}).callPackage ./pkgs/mods/geoguessr/default.nix { };
          duels = (nixpkgsFor.${system}).callPackage ./pkgs/mods/duels/default.nix { };
          bowser-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/bowser/default.nix { };
          super-mario-rpg-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/smrpg/default.nix { };
          sonic-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/sonic-character-rebooted/default.nix { };
          tag = (nixpkgsFor.${system}).callPackage ./pkgs/mods/tag/default.nix { };
          sonic-adventure-64-dx = (nixpkgsFor.${system}).callPackage ./pkgs/mods/sonic-adventure-64-dx/default.nix { };
          star-revenge-0_5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/0.5.nix { };
          star-revenge-1 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/1.nix { };
          star-revenge-1_3 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/1.3.nix { };
          star-revenge-1_5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/1.5.nix { };
          star-revenge-2 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/2.nix { };
          star-revenge-2_5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/2.5.nix { };
          star-revenge-3 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/3.nix { };
          star-revenge-4 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/4.nix { };
          star-revenge-4_5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/4.5.nix { };
          star-revenge-5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/5.nix { };
          star-revenge-6 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/6.nix { };
          star-revenge-6_9 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/6.9.nix { };
          star-revenge-7 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/7.nix { };
          star-revenge-7_5 = (nixpkgsFor.${system}).callPackage ./pkgs/mods/star-revenge/7.5.nix { };
          pizza-pack = (nixpkgsFor.${system}).callPackage ./pkgs/mods/pizza-pack/default.nix { };
          maurice-spaghetti = (nixpkgsFor.${system}).callPackage ./pkgs/mods/maurice-spaghetti/default.nix { };
          mega-man-x = (nixpkgsFor.${system}).callPackage ./pkgs/mods/mega-man-x/default.nix { };
          azumanga-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/azumanga-character-select/default.nix { };
          peter-shorts = (nixpkgsFor.${system}).callPackage ./pkgs/mods/peter-shorts/default.nix { };
          emmy-the-robot-character-select = (nixpkgsFor.${system}).callPackage ./pkgs/mods/emmy-the-robot/default.nix { };
          emmy-the-robot-modelpack = (nixpkgsFor.${system}).callPackage ./pkgs/mods/emmy-the-robot/default.nix { modelType = "dynos"; };
        });
    };
}
