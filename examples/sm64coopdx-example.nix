{ mksm64coopdx
, sm64pkgs
}: mksm64coopdx { 
  # The name of the launch script.
  name = "sm64coopdx-example";

  # Place your personal, legal, copy of the Super Mario 64 (US) ROM at ../dummyrom.z64
  rom = ../dummyrom.z64;

  # Optionally, choose one or more of the following model (aka. DynOS) packs.
  modelPacks = [
    # sm64pkgs.hanyuu-modelpack
  ];

  # Optionally, choose one or more of the following Lua mods.
  mods = [
    # sm64pkgs.javoni-character-select
    # sm64pkgs.klonoa-character-select
    # sm64pkgs.widdle-pets
    # sm64pkgs.sponge-bob-character-select
    # sm64pkgs.the-underworld
    # sm64pkgs.geoguessr
    # sm64pkgs.duels
    # sm64pkgs.bowser-character-select
    # sm64pkgs.super-mario-rpg-character-select
    # sm64pkgs.sonic-character-select
    # sm64pkgs.tag
    # sm64pkgs.sonic-adventure-64-dx
    # sm64pkgs.star-revenge-0_5
    # sm64pkgs.star-revenge-1
    # sm64pkgs.star-revenge-1_3
    # sm64pkgs.star-revenge-1_5
    # sm64pkgs.star-revenge-2
    # sm64pkgs.star-revenge-2_5
    # sm64pkgs.star-revenge-3
    # sm64pkgs.star-revenge-4
    # sm64pkgs.star-revenge-4_5
    # sm64pkgs.star-revenge-5
    # sm64pkgs.star-revenge-6
    # sm64pkgs.star-revenge-6_9
    # sm64pkgs.star-revenge-7
    # sm64pkgs.star-revenge-7_5
    # sm64pkgs.pizza-pack
    # sm64pkgs.maurice-spaghetti
    # sm64pkgs.mega-man-x
    # sm64pkgs.azumanga-character-select
    # sm64pkgs.peter-shorts
    # sm64pkgs.emmy-the-robot-character-select
  ];
 
  # By default, X11 is always used for rendering; When using a Wayland compositor Xwayland is used.
  # But when this option is set to `true`, Wayland is used for rendering if it's available, falling back to X11.
  enableWayland = false;
}
