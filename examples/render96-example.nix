{ mkrender96
, sm64pkgs
}: mkrender96 { 
  # The name of the launch script.
  name = "render96-example";

  # Place your personal, legal, copy of the Super Mario 64 (US) ROM at ../dummyrom.z64
  rom = ../dummyrom.z64;

  # Optionally, choose one or more of the following model packs.
  modelPacks = [
    sm64pkgs.render96-modelpack
    # sm64pkgs.hanyuu-modelpack
    # sm64pkgs.analogue-brothers-modelpack
    # sm64pkgs.jumpman-modelpack
    # sm64pkgs.emmy-the-robot-modelpack
  ];

  # Optionally, choose a texture pack by uncommenting ONE of the following:
  texturePack = sm64pkgs.render96-texturepack;
  # texturePack = sm64pkgs.resrgan-texturepack;
  # texturePack = sm64pkgs.p3st-texturepack;
  # texturePack = sm64pkgs.sm64redrawn-texturepack;
  # texturePack = sm64pkgs.sm64redrawn-alt-texturepack;
  # texturePack = sm64pkgs.super-mario-64-hds-texturepack;
  # texturePack = sm64pkgs.sm64reloaded-texturepack;
 
  # Since it can take a while for `render96` to load, by default a notification is emitted to let you know something is happening.
  # If you don't want this notification, set `enableNotification` to `false`.
  enableNotification = true;

  # By default, X11 is always used for rendering; When using a Wayland compositor Xwayland is used.
  # But when this option is set to `true`, Wayland is used for rendering if it's available, falling back to X11.
  enableWayland = false;
}
