# sm64nix

sm64nix provides a reproducible way to build [sm64](https://github.com/sm64-port/sm64-port) forks for Linux x864-64. Currently, sm64nix can build the following forks:

- [render96ex](https://github.com/Render96/Render96ex)
- [SM64 Coop Deluxe](https://github.com/coop-deluxe/sm64coopdx)

By using the [Nix](https://nixos.org/guides/how-nix-works/) package manager and the [Nixpkgs](https://github.com/NixOS/nixpkgs) repository, sm64nix can build sm64 using nothing more than a simple specification file. Here's an example of a specification to build Render96 with the Render96 HD models and textures:


```
{ mkrender96
, sm64pkgs
}: mkrender96 { 
  name = "render96-example";
  rom = ./sm64.z64;
  modelPacks = [ sm64pkgs.render96-modelpack ];
  texturePack = sm64pkgs.render96-texturepack;
}
```

You simply provide the sm64 US ROM and a specification like the one shown above, and sm64nix takes care of the rest.

Once you've build sm64 using sm64nix, you can run sm64 on the same Linux x86-64 system. Alternatively, you can use sm64nix to build an AppImage, which you can then copy to another one of your PCs!

## How does it work?

sm64nix is a [Nix flake](https://wiki.nixos.org/wiki/Flakes) containing Nix functions to build packages of sm64. It was primarily designed for building and running sm64 on NixOS. However, it can also be used to build and run sm64 on other Linux x86-64 distributions.

## How to use sm64nix

There are different ways to use sm64nix. The process mostly depends on whether you are using the NixOS Linux distribution, or not.

### NixOS with Nix flakes

The simplest use-case is when you're using a NixOS flake. If you are using NixOS without Nix flakes, then unfortunately you won't be able use use sm64nix at this time.

Start by adding sm64nix to your flake inputs:

```
{
  ...
  inputs.sm64nix.url = "gitlab:emmanuelrosa/sm64nix";
  inputs.sm64nix.inputs.nixpkgs.follows = "nixpkgs";
  ...

  outputs = { self, nixpkgs, sm64nix, ...}: {
   ...
  }
}
```

Then, in your NixOS configuration, use a Nix function, such as `mkrender96`, to configure and install your desired sm64 package:

```
environment.systemPackages = let
  sm64pkgs = sm64nix.packages."x86_64-linux";
  sm64lib = sm64nix.lib."x86_64-linux";
in [ 
  ...
  (sm64lib.mkrender96 { 
      name = "render96";
      rom = ./sm64rom.z64;
      modelPacks = [ sm64pkgs.render96-modelpack ];
      texturePack = sm64pkgs.render96-texturepack;
  });
];
```

The idea demonstrated above can also be used with [Home Manager](https://github.com/nix-community/home-manager), but keep in mind that sm64nix is a Nix flake without backward-compatibility with flake-less Nix; Backward-compatibility is possible, I just haven't bothered.

Next, `git add` your Super Mario 64 (US) ROM to your NixOS flake. The `rom` attribute should reflect the file name of the ROM file (ex. `sm64rom.z64`).

After you rebuild your NixOS system with `nixos-rebuild switch`, you'll have an executable named according to the `name` attribute (ex. `render96`). Go ahead a run that to enjoy sm64!

To see more examples, take a look at the `examples` directory in this repo.

### Other Linux x86-64 distributions

This section is for those of you not using the NixOS Linux distribution. For you I suggest using the `sm64nix-easy.bash` script, which is located in the `easy` directory of this repository. Watch it in action!

[![asciicast](https://asciinema.org/a/DVtWDuKq0BLvTwAtf6ewYxd1o.svg)](https://asciinema.org/a/DVtWDuKq0BLvTwAtf6ewYxd1o)

With this method you'll need:

- bash
- curl
- coreutils
- Nix (package manager); `sm64nix-easy.bash` Can download the installer for you.
- Your sm64 US ROM.

This method entails installing the Nix package manager using the Determine Systems Nix installer; See https://github.com/DeterminateSystems/nix-installer

This is the most reliable and cleanest method of installing (and uninstalling) the Nix package manager. Alternatively, there's a way to convince `sm64nix-easy.bash` to use [Nix portable](https://github.com/DavHau/nix-portable) instead, but I haven't been successful with this method.

To proceed with this method, simply download `sm64nix-easy.bash` from the `easy` directory, make the script executable, and then run it from a terminal. The script will guide you through the whole process.

## What's included?

sm64nix includes sm64 forks, texture packs, (DynOS) model packs, and SM64 Coop Deluxe mods. You can see exactly what's included by looking in the `pkgs` directory.

## Limitations

There are a number of limitations to sm64nix which you should be aware of. These limitations don't apply when using NixOS.

### sm64nix requires the Nix package manager

I can understand if you're hesitant to install another package manager to your Linux distro. In such a case, you can create a virtual machine and do the build there. I recommend using an Ubuntu VM with a 20GB virtual disk, 4GB of RAM, and a graphical environment so you can test your build specification. Don't expect great performance even when using virtio and virgl. When you have the build you want, you can build an AppImage out of it and copy that to your gaming PC.

`sm64nix-easy` can build the AppImage.

NOTE: You should be able to run sm64 with about 2GB of RAM if you don't go crazy with HD textures, but you'll need about 4GB of RAM to build it.

### Mesa-compatible GPUs only

Due to the way NixOS handles OpenGL drivers, the packages built by sm64nix need to include the OpenGL drivers. You still need the same drivers installed on your system to please Xorg, but sm64nix uses only the bundled Mesa drivers. This bundling is done using [nanoGL](https://github.com/emmanuelrosa/erosanix/blob/master/lib/nanogl.nix), which is my minimalist fork of [nixGL](https://github.com/guibou/nixGL).

It's technically possible to also support the NVIDIA proprietary drivers, but I haven't gotten around to it. Note that NVIDIA GPUs are supported just fine on NixOS; It's what I use with my hybrid-graphics laptop.

### The AppImage is quite large

The AppImage is built with Nix, rather than with the normal AppImage tools. This has the benefit of creating an AppImage containing *all* of the sm64 runtime dependencies; From sm64 itself down to glibc. This makes the AppImage more portable than if it were built with the expectation of a minimal set of libraries provided by the host, as is common practice. But the consequence of this is that the AppImage can be quite large. For example, an AppImage of Render 96 with the HD models and textures weighs in at about 1.5GB.

### The AppImage needs Linux User Namespaces support

Since the AppImage is built with Nix, all of the content is stored in the Nix store, which is expected to be mounted at `/nix`. The AppImage contains this Nix store and needs to mount it a `/nix`, which of course a user process can't do. Therefore, the AppImage runtime uses Linux User Namespaces to create a "Linux container," which allows for the Nix store to be mounted at the expected location. Some Linux distributions may prohibit Linux User Namespaces by default, which of course prevents the AppImage from functioning. For more details about how the AppImage works, see [nix-appimage](https://github.com/ralismark/nix-appimage).
