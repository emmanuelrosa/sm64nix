#! /usr/bin/env bash
# vim: set tabstop=2 shiftwidth=2 softtabstop=2:

arch=$(uname -m)
curl=$(which curl 2>/dev/null)
git=$(which git 2>/dev/null)
nix=$(which nix 2>/dev/null)
sha1sum=$(which sha1sum 2>/dev/null)

work_dir=$HOME/sm64nix-easy
sm64nix_url="https://gitlab.com/emmanuelrosa/sm64nix.git"
nix_installer=$work_dir/nix-installer
nix_installer_version="0.20.0"
nix_portable_version="012"
nix_portable=$work_dir/nix-portable
nix_portable_force=$work_dir/force-nix-portable
sm64nix_dir=$work_dir/sm64nix
appimage=$work_dir/render96-$arch.AppImage

print_usage() {
  echo "USAGE: sm64nix-easy.bash COMMAND"
  echo "Commands include:"
  echo
  echo "render96               - Build and run Render 96"
  echo "render96-appimage      - Build a Render 96 AppImage"
  echo "sm64coopdx             - Build and run SM64 Coop Deluxe"
  echo "sm64coopdx-appimage    - Build a SM64 Coop Deluxe AppImage"
}

if [ $arch != "x86_64" ]
  then
    echo "ERROR: Architecture $arch is not supported."
    exit 1
  fi

subcommand=$1

# Determine what to do.
if [ "$subcommand" == "render96" ]
  then
    echo "INFO: using $subcommand command"
    letsgo_flag=$sm64nix_dir/letsgo-render96
    spec_file="render96-example.nix"
elif [ "$subcommand" == "render96-appimage" ]
  then
    echo "INFO: using $subcommand command"
    letsgo_flag=$sm64nix_dir/letsgo-render96
    spec_file="render96-example.nix"
elif [ "$subcommand" == "sm64coopdx" ]
  then
    echo "INFO: using $subcommand command"
    letsgo_flag=$sm64nix_dir/letsgo-sm64coopdx
    spec_file="sm64coopdx-example.nix"
elif [ "$subcommand" == "sm64coopdx-appimage" ]
  then
    echo "INFO: using $subcommand command"
    letsgo_flag=$sm64nix_dir/letsgo-sm64coopdx
    spec_file="sm64coopdx-example.nix"
else
  print_usage
  exit
fi

# Check for curl. One of the very few dependencies for sm64nix-easy
if [ -z "$curl" ]
  then
    echo "ERROR: curl not found! Please install curl using your package manager, and then try again."
    exit 1
  fi

# Check for sha1sum (coreutils). One of the very few dependencies for sm64nix-easy
if [ -z "$sha1sum" ]
  then
    echo "ERROR: sha1sum not found! Please install coreutils using your package manager, and then try again."
    exit 1
  fi

mkdir -p $work_dir

if [ -z "$nix" ]
  then
    if [ -f $nix_portable_force ]
      then
        echo "WARNING: Proceeding with nix-portable, but beware it has a lower success rate."

        # Download nix-portable, if it's missing.
        if [ ! -f $nix_portable ]
          then
            $curl -L https://github.com/DavHau/nix-portable/releases/download/v$nix_portable_version/nix-portable-$arch > $nix_portable
            chmod u+x $nix_portable
          fi
      else
        if [ ! -f $nix_installer ]
          then
            echo "Downloading the Nix installer."
            $curl -L https://github.com/DeterminateSystems/nix-installer/releases/download/v$nix_installer_version/nix-installer-$arch-linux > $nix_installer
            chmod u+x $nix_installer
          fi

        echo "The 'Determinate Systems' Nix installer has been downloaded to $nix_installer"
        echo "Execute the installer to install Nix. Then run: sm64nix-easy.bash $subcommand"
        exit
      fi
  else
    echo "INFO: Using your installed Nix package manager."
  fi

# Download a clone of sm64nix, if it's missing.
if [ ! -d $sm64nix_dir ]
  then
    if [ -z "$git" ]
      then
	      if [ -z "$nix" ]      
          then
            # Use nix-portable to run git and clone sm64nix
            $nix_portable nix run 'nixpkgs#git' -- clone $sm64nix_url $sm64nix_dir
          else
            # Use Nix to run git and clone sm64nix
            $nix run 'nixpkgs#git' -- clone $sm64nix_url $sm64nix_dir
          fi
      else
        # Use the host's git to clone sm64nix
        $git clone $sm64nix_url $sm64nix_dir
      fi
  fi

# Look for sm64 ROM
pushd $sm64nix_dir
checksum=$(sha1sum -c dummyrom.z64.us.sha1)
popd

if [ "$checksum" != "dummyrom.z64: OK" ]
  then
    echo "Replace the file $sm64nix_dir/dummyzom.z64 with your copy of the Super Mario 64 (US) ROM. Then try again."
    exit
  fi

# Look for the "Let's Go" flag for confirmation from the user to (build and) run Render 96
if [ ! -f $letsgo_flag ]
  then
    echo "Edit the build specification file $sm64nix_dir/examples/$spec_file" 
    echo "When you're done, execute the command: touch $letsgo_flag"
    echo "Then run: sm64nix-easy.bash $subcommand"
    exit
  fi

export NIXPKGS_ALLOW_UNFREE=1

if [ "$subcommand" == "render96-appimage" ]
  then
    echo "Building the Render 96 AppImage..."
    pushd $sm64nix_dir

    if [ -z "$nix" ]
      then
        # Use nix-portable to build an AppImage for Render 96.
        $nix_portable nix build '.#render96-nanogl-example-appimage' --impure
      else
        # Use Nix to build an AppImage for Render 96.
        $nix build '.#render96-nanogl-example-appimage' --impure
      fi

    popd

    if [ -h $sm64nix_dir/result ]
      then
        cp -L $sm64nix_dir/result $appimage
        echo "Your AppImage is ready at $appimage"
      else
        echo "Oh no! It looks like something went wrong. Sorry!"
      fi
  fi

if [ "$subcommand" == "render96" ]
  then
    echo "Building Render 96..."
    pushd $sm64nix_dir

    if [ -z "$nix" ]
      then
        # Use nix-portable to (build and) run Render 96
        $nix_portable nix run '.#render96-nanogl-example' --impure
      else 
        # Use Nix to (build and) run Render 96
        $nix run '.#render96-nanogl-example' --impure
      fi

    popd
  fi

if [ "$subcommand" == "sm64coopdx-appimage" ]
  then
    echo "Building the SM64 Coop Deluxe AppImage..."
    pushd $sm64nix_dir

    if [ -z "$nix" ]
      then
        # Use nix-portable to build an AppImage for Render 96.
        $nix_portable nix build '.#sm64coopdx-nanogl-example-appimage' --impure
      else
        # Use Nix to build an AppImage for Render 96.
        $nix build '.#sm64coopdx-nanogl-example-appimage' --impure
      fi

    popd

    if [ -h $sm64nix_dir/result ]
      then
        cp -L $sm64nix_dir/result $appimage
        echo "Your AppImage is ready at $appimage"
      else
        echo "Oh no! It looks like something went wrong. Sorry!"
      fi
  fi

if [ "$subcommand" == "sm64coopdx" ]
  then
    echo "Building SM64 Coop Deluxe..."
    pushd $sm64nix_dir

    if [ -z "$nix" ]
      then
        # Use nix-portable to (build and) run Render 96
        $nix_portable nix run '.#sm64coopdx-nanogl-example' --impure
      else 
        # Use Nix to (build and) run Render 96
        $nix run '.#sm64coopdx-nanogl-example' --impure
      fi

    popd
  fi
