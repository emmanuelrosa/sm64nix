{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "RENDER96-HD-TEXTURE-PACK";
  version = "70a7066907996fe0447d521279c11cc01fc9dba9";

  src = fetchFromGitHub {
    owner = "pokeheadroom";
    repo = pname;
    rev = version; 
    sha256 = "sha256-YzQMmKRkUPIMtLvklgqYZC6xMW4bn+j8ZesQ7ZoRDIA=";
  };

  installPhase = ''
    mkdir -p $out/res
    cp -r gfx $out/res/
  '';

  meta = with lib; {
    description = "Render96 texture pack for Super Mario 64.";
    homepage = "https://github.com/pokeheadroom/RENDER96-HD-TEXTURE-PACK";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

