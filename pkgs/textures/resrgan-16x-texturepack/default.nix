{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "RESRGAN-16xre-upscale-HD-texture-pack";
  version = "3.0";

  src = fetchFromGitHub {
    owner = "pokeheadroom";
    repo = pname;
    rev = version; 
    sha256 = "sha256-GGFNlYIaXl/bisHbJsOTb27zBmpYzx6LdDHxhmyBNWo=";
  };

  installPhase = ''
    mkdir -p $out/res
    cp -r gfx $out/res/
  '';

  meta = with lib; {
    description = "A texture pack for super mario 64 that is made of a compilation of the best results esrgan upscaling could make as well of various other improvements";
    homepage = "https://github.com/pokeheadroom/RESRGAN-16xre-upscale-HD-texture-pack";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

