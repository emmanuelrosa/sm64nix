{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "sm64reloaded-texturepack";
  version = "b42a9d246229a34ccfa42ea0d3add8dcc7812319"; 

  src = fetchFromGitHub {
    owner = "GhostlyDark";
    repo = "SM64-Reloaded-PC";
    rev = version; 
    sha256 = "sha256-+o9SokAd6/YWeTSSwxNNR1GLNZGtLr1fGQMZ2KeM4yo=";
  };

  installPhase = ''
    mkdir -p $out/res/gfx
    cp -r gfx/. $out/res/gfx/
  '';

  meta = with lib; {
    description = "An Ultra HD texture pack for GLideN64, Dolphin and the PC port.";
    homepage = "https://github.com/GhostlyDark/SM64-Reloaded-PC";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

