{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "Super-Mario-64-HDS";
  version = "80a522eb14cf2c578e90ef202f582b46d240fb13";

  src = fetchFromGitHub {
    owner = "pokeheadroom";
    repo = pname;
    rev = version; 
    sha256 = "sha256-vsstkZqINOfIhYsPR6B9zG014okDQo8KlnY3n4C9njA=";
  };

  installPhase = ''
    mkdir -p $out/res
    cp -r gfx $out/res/
  '';

  meta = with lib; {
    description = "A mod for Super Mario 64 (The PC Port Specifically) it's one that imports and upscales Mario 64 DS textures by a factor of 16, it is conceptually sort of a mix between the Cleaner Aesthetics and the RESRGAN 16x re-upscale texturepacks.";
    homepage = "https://github.com/pokeheadroom/Super-Mario-64-HDS";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

