{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "p3st-texturepack";
  version = "9a15c15e0f016b56aa89b006260fac0f3482c99b";

  src = fetchFromGitHub {
    owner = "p3st-textures";
    repo = "p3st-Texture_pack";
    rev = version; 
    sha256 = "sha256-qjls8yz/efwcO83lwiWe2kaeCO1AINzz5WfjDKB+Zmw=";
  };

  installPhase = ''
    mkdir -p $out/res
    cp -r gfx $out/res/
    cp -r sound $out/res/
  '';

  meta = with lib; {
    description = "A Mario 64 HD texture project";
    homepage = "https://github.com/p3st-textures/p3st-Texture_pack";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

