{ lib
, stdenv
, fetchFromGitHub
, useAlternativeTextures ? false
}: stdenv.mkDerivation rec {
  pname = if useAlternativeTextures then "sm64redrawn-alt-texturepack" else "sm64redrawn-texturepack";
  version = "1.3.1";

  src = fetchFromGitHub {
    owner = "TechieAndroid";
    repo = "sm64redrawn";
    rev = "v${version}"; 
    sha256 = "sha256-2ggEdiDr6phM+B/cpr65l5hYx2kL3c7ZmSmyO5Nq2H4=";
  };

  installPhase = let
    gfxDir = if useAlternativeTextures then "alt" else "gfx";
  in ''
    mkdir -p $out/res/gfx
    cp -r ${gfxDir}/. $out/res/gfx/
  '';

  meta = with lib; {
    description = "This is a collection of textures that have been redrawn for sm64";
    homepage = "https://github.com/TechieAndroid/sm64redrawn";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

