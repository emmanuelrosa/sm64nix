# NOTE: You probably shouldn't use this package directly.
# Instead, use lib.mksm64coopdx
{ lib
, stdenv
, glew
, SDL2
, python3
, fetchFromGitHub
, util-linux
, curl
, symlinkJoin
, alsa-lib
, libpulseaudio
, patchelf
, rom
, enableTextureFix ? false
}: stdenv.mkDerivation rec {
  pname = "sm64coopdx-unwrapped";
  version = "1.0.1";

  src = fetchFromGitHub {
    owner = "coop-deluxe";
    repo = "sm64coopdx";
    rev = "v${version}"; 
    sha256 = "sha256-M/LAaeelB68+8QVg0nqEuj49JAwUbELwy/gXBwiHhJQ=";
  };

  buildInputs = [ glew SDL2 curl alsa-lib libpulseaudio ];
  nativeBuildInputs = [ python3 util-linux patchelf ];
  hardeningDisable = [ "format" ];
  makeFlags = [] ++ (if enableTextureFix then [ "TEXTURE_FIX=1" ] else []);
  enableParallelBuilding = true;
  maxBuildCores = 4;
  patches = [ ./miniaudio.patch ];

  env.linuxAudioLibs = symlinkJoin {
    name = "${pname}-linux-audio-libs";
    paths = [ "${alsa-lib}/lib" "${libpulseaudio}/lib" ];
  };

  configurePhase = ''
    cp ${rom} ./baserom.us.z64
    patchShebangs extract_assets.py
    substituteAllInPlace Makefile
    substituteAllInPlace src/pc/utils/miniaudio.h
    mkdir -p build/us_pc/res
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/${pname}
    mkdir -p $out/share/icons/hicolor/16x16/apps/
    cp ${rom} $out/share/${pname}/baserom.us.z64
    cp -r build/us_pc/. $out/share/${pname}/
    cp actors/mario/custom_mario_logo.rgba16.png $out/share/icons/hicolor/16x16/apps/${pname}.png
    patchelf --add-rpath $out/share/${pname} $out/share/${pname}/sm64coopdx

    runHook postInstall
  '';

  meta = with lib; {
    description = "An online multiplayer project for the Super Mario 64 PC port, started by the Coop Deluxe Team.";
    longDescription = "Its purpose is to actively maintain and improve, but also to continue sm64ex-coop, an original idea from djoslin0. More features, customizability, and power to the Lua API allow modders and players to enjoy Super Mario 64 more than ever!";
    homepage = "https://sm64coopdx.com/";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}
