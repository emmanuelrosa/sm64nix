# NOTE: You probably shouldn't use this package directly.
# Instead, use lib.mkrender96
{ lib
, stdenv
, glew
, SDL2
, audiofile
, python3
, fetchFromGitHub
, util-linux
, ensureNewerSourcesForZipFilesHook
, rom
}: stdenv.mkDerivation rec {
  pname = "render96ex-unwrapped";
  version = "06f2594fbecc699dc84a4334d80d53e17401fd4f"; # alpha branch

  src = fetchFromGitHub {
    owner = "Render96";
    repo = "Render96ex";
    rev = version; 
    sha256 = "sha256-RbeUzYK5PeKN7vJT95MFL+t5zBd7hSqh7YdKGGlWHQI=";
  };

  buildInputs = [ glew SDL2 audiofile ];
  nativeBuildInputs = [ python3 util-linux ensureNewerSourcesForZipFilesHook ];
  hardeningDisable = [ "format" ];
  makeFlags = [ "EXTERNAL_DATA=1" "TEXTURE_FIX=1" "BETTERCAMERA=1" ];
  patches = [ ./Makefile.patch ];
  enableParallelBuilding = true;
  maxBuildCores = 4;

  configurePhase = ''
    cp ${rom} ./baserom.us.z64
    patchShebangs extract_assets.py
    substituteAllInPlace Makefile
    mkdir -p build/us_pc/res
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/${pname}
    mkdir -p $out/share/icons/hicolor/16x16/apps/
    cp build/us_pc/sm64.us.f3dex2e $out/share/${pname}/
    cp -r build/us_pc/res $out/share/${pname}/
    cp -r build/us_pc/dynos $out/share/${pname}/
    cp actors/mario/logo.rgba16.png $out/share/icons/hicolor/16x16/apps/${pname}.png

    runHook postInstall
  '';

  meta = with lib; {
    description = "Fork of https://github.com/sm64-port/sm64-port with additional features.";
    homepage = "https://github.com/Render96/Render96ex";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}
