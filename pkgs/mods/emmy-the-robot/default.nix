{ lib
, stdenv
, fetchurl
, unzip
, modelType ? "character-select" # Can be 'character-select' or 'dynos'.
}: stdenv.mkDerivation rec {
  pname = "emmy-the-robot-${modelType}";
  version = "2024-07-10";

  srcs = [
    (fetchurl {
      name = "emmy-the-robot.zip";
      url = "https://mods.sm64coopdx.com/mods/cs-and-dynos-emmy-the-robot-extra-nandroids.251/version/367/download?file=1126";
      sha256 = "sha256-xhegJ48bxcs015DYOJJ2Oi44DWde7nssITaln5igXdY=";
    })

    (fetchurl {
      name = "emmy-the-robot-nandroids.zip";
      url = "https://mods.sm64coopdx.com/mods/cs-and-dynos-emmy-the-robot-extra-nandroids.251/version/367/download?file=1127";
      sha256 = "sha256-5OykwEScxstgnfts5+kSadE89/YhJwFgfhksigNBEWs=";
    })
  ];

  nativeBuildInputs = [ unzip ];

  unpackCmd = ''
    mkdir cs
    pushd cs
    unzip $curSrc
    popd
  '';

  installPhase = {
    "character-select" = ''
      mkdir -p $out/emmy-the-robot
      mkdir -p $out/emmy-the-robot-nandroids
      cp -r "EmmytheRobot 64/char-select-emmy/." $out/emmy-the-robot/
      cp -r "Nandroids 64/char-select-extra-nandroids/." $out/emmy-the-robot-nandroids/
    '';

    "dynos" = ''
      mkdir -p $out/dynos/packs
      cp -r "EmmytheRobot 64/DynOS/." $out/dynos/packs/
      cp -r "Nandroids 64/DynOS/." $out/dynos/packs/
    '';
  }."${modelType}";

  meta = with lib; {
    description = "Featuring characters from the webcomic Emmy the Robot by Dominic Cellini!";
    homepage = "https://mods.sm64coopdx.com/mods/cs-and-dynos-emmy-the-robot-extra-nandroids.251/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

