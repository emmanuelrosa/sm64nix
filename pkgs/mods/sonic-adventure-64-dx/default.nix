{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "sonic-adventure-64-dx";
  version = "1.0";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/sonic-adventure-64-dx.135/download";
    sha256 = "sha256-A6n36zvCofKrZPL973mkOd90Xo/4dA2jzHgwG29LY84=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Sonic Adventure ROM hack for sm64 Coop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/sonic-adventure-64-dx.135/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

