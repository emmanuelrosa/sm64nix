{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "klonoa-character-select";
  version = "1.0";

  src = fetchurl {
    name = "klonoa.zip";
    url = "https://mods.sm64coopdx.com/mods/klonoa.235/version/328/download";
    sha256 = "sha256-Lcb4pxBVnO8ifi2tauX7+YTWmslGbyKu1mAQ86cy398=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/Klonoa
    cp -r . $out/Klonoa/
  '';

  meta = with lib; {
    description = "Klonoa character selection mod for SM64 Cooop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/klonoa.235/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

