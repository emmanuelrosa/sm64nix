{ lib
, stdenv
, fetchFromGitHub
}: stdenv.mkDerivation rec {
  pname = "azumanga-character-select";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "LukasATBK";
    repo = "char-select-azumanga-pack";
    rev = version; 
    sha256 = "sha256-YAABKVfpQY7VEHuQQsHcaSmcA0D5QscPn5mrD/YBX7c=";
  };

  outputs = [ "out" "doc" ];

  installPhase = ''
    mkdir -p $out/${pname}
    mkdir -p $doc/${pname}
    cp -r . $out/${pname}/
    mv $out/${pname}/README.md $doc/${pname}/
    rm $out/${pname}/.gitattributes
  '';

  meta = with lib; {
    description = "A Model Pack for SM64CoopDX and Character Select adding characters from Azumanga Daioh!";
    homepage = "https://github.com/LukasATBK/char-select-azumanga-pack";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

