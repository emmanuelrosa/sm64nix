{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "the-underworld";
  version = "1.1.2";

  src = fetchurl {
    name = "underworld.zip";
    url = "https://mods.sm64coopdx.com/mods/super-mario-64-the-underworld.1/version/315/download";
    sha256 = "sha256-f45ae/X2UHDKia8+Qd7Fw71+MI3g0Otm5wDTmuHN0eo=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/the-underworld
    cp -r . $out/the-underworld/
  '';

  meta = with lib; {
    description = "Mario is pulled into another land some call The Underworld.";
    longDescription = "Mario must make his way through this condemned land and help in both the escape of himself and someone he thinks he can trust from the Underworld... This is a 30 star rom hack with a fully custom cutscene system, dialog system and boss fight entirely in Lua created for the sm64ex-coop level competition.";
    homepage = "https://mods.sm64coopdx.com/mods/super-mario-64-the-underworld.1/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

