{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "javoni-character-select";
  version = "2024-06-28";

  src = fetchurl {
    url = "https://files.gamebanana.com/mods/cs_javoni.zip";
    sha256 = "sha256-S4xMTk3wpCLyiTfblcn9khazIxhqWo7f7pqDaJ2VDdY=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/Javoni
    cp -r . $out/Javoni/
  '';

  meta = with lib; {
    description = "Javoni character selection mod for SM64 Cooop Delux";
    homepage = "https://gamebanana.com/mods/518096";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

