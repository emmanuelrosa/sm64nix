{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "sonic-character-select";
  version = "1.2.1";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/sonic-character-rebooted.13/version/311/download";
    sha256 = "sha256-PI+5EBU762Is/So5M/fwa5iKV2TkzWkv8LUf/D06OxI=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/SonicCharacterRebooted
    cp -r . $out/SonicCharacterRebooted/
  '';

  meta = with lib; {
    description = "Sonic and friends character selection mod for SM64 Cooop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/sonic-character-rebooted.13/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

