{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "super-mario-rpg-character-select";
  version = "1.2.1";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/cs-smrpg-characters.35/version/248/download";
    sha256 = "sha256-A8spBy3AVR1DsE/PtLtQCqSljx1XquamshQ66M3NGaE=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/smrpg

    cp -r . $out/smrpg/
  '';

  meta = with lib; {
    description = "Super Mario RPG character selection mod for SM64 Cooop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/cs-smrpg-characters.35/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

