{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "mega-man-x";
  version = "1.1.5";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/mega-man-x.167/version/346/download?file=1085";
    sha256 = "sha256-k9AurkDoG3/eQ4uqfKr2BTHH58eJPWT3pJ9jupWssEk=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Mega Man X moveset for sm64 Coop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/mega-man-x.167/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

