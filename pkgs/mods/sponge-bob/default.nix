{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "sponge-bob-character-select";
  version = "2024-05-25";

  src = fetchurl {
    name = "sponge-bob-squarepants.zip";
    url = "https://mods.sm64coopdx.com/mods/cs-voiced-spongebob.186/version/241/download";
    sha256 = "sha256-ODZaSnIgVD22E95cieB+lEgDuekgxYsxapOBjBgIXl8=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/SpongeBob
    cp -r . $out/SpongeBob/
  '';

  meta = with lib; {
    description = "Sponge Bob Squarepants selection mod for SM64 Cooop Deluxe";
    homepage = "https://mods.sm64coopdx.com/mods/cs-voiced-spongebob.186/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

