{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "geoguessr";
  version = "2024-07-02";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/geoguessr-scavenger-hunt.188/version/319/download";
    sha256 = "sha256-8EVDeiPTbtqfD55LQZHhx929yWfg6WEQNvG1C77HGNw=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "GeoGuessr implemented into Super Mario 64!";
    longDescription = "This game mode is somewhat similar to Hide and Seek, with a much greater emphasis on the 'Hide'. One player hides anywhere in the game, and the other players have to find them using picture/word clues.";
    homepage = "https://mods.sm64coopdx.com/mods/geoguessr-scavenger-hunt.188/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

