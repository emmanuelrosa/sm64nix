{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "maurice-spaghetti";
  version = "1.0";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/cs-maurice-spaghetti.238/download";
    sha256 = "sha256-NCh26KxlVyYvPoQ+MbqzfB1IhfDmjFLF99UiKLTgwZA=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Here's Maurice Spaghetti, Peppino's highly judgemental brother!";
    longDescription = "He dislikes his Italian heritage, loves to gamble, and has a history of violent outbursts with his iconic sack of nickels. He always disapproves of Peppino's life choices and has a complicated relationship with him, however he oddly gets along with The Noise.";
    homepage = "https://mods.sm64coopdx.com/mods/cs-maurice-spaghetti.238/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

