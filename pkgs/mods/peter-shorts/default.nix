{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "peter-shorts";
  version = "2024-07-13";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/cs-peter-shorts.237/version/382/download";
    sha256 = "sha256-Srwz5ekC1PFV1kEt5z+rNvX627cSPnYf+3d1qaq4Cpk=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Play as Peter Shorts from the game You Are Peter Shorts. He has custom voice lines, and is customizable via palettes!";
    homepage = "https://mods.sm64coopdx.com/mods/cs-peter-shorts.237/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

