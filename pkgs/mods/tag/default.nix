{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "tag";
  version = "2.4";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://github.com/EmeraldLoc/tag/releases/download/v${version}/${pname}.zip";
    sha256 = "sha256-Ji85lSVvMUBWX4H4P1aH5WeHPGrEBxzpWV0ctci1FAg=";
  };

  nativeBuildInputs = [ unzip ];

  outputs = [ "out" "doc" ];

  unpackCmd = ''
    mkdir ${pname}
    pushd ${pname}
    unzip $curSrc
    chmod -R ugo+r .
    popd
  '';

  installPhase = ''
    mkdir -p $out/${pname}
    mkdir -p $doc/${pname}

    cp -r ./tag/. $out/${pname}/
    rm -fR $out/${pname}/.DS_Store
    mv $out/${pname}/README.md $doc/
    mv $out/${pname}/docs/ $doc/
  '';

  meta = with lib; {
    description = "Tag contains multiple tag-related gamemodes! All gamemodes involve 'tagging' another player. ";
    longDescription = "There are as of right now 12 different gamemodes. These gamemodes are Tag, Freeze Tag, Infection, Hot Potato, Juggernaut (meh), Assassins, Sardines, Hunt, Deathmatch, Terminator, and Oddball!";
    homepage = "https://github.com/EmeraldLoc/tag";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

