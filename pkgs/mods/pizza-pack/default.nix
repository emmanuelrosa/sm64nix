{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "pizza-pack";
  version = "1.11";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/cs-pizza-pack.233/version/392/download";
    sha256 = "sha256-0TYGWWO2H3vp1IK5zDxCn7Jljdn8AM4EzYw2cZ6NeAk=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Finally, the mod everyone's been waiting for...! Pizza Pack for Character Select, feat. Peppino, Gustavo, Noise and Noisette from Pizza Tower, all fully voiced!";
    homepage = "https://mods.sm64coopdx.com/mods/cs-pizza-pack.233/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

