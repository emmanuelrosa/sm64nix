{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "duels";
  version = "2024-07-03";

  src = fetchurl {
    name = "duels.zip";
    url = "https://mods.sm64coopdx.com/mods/duels.9/version/320/download";
    sha256 = "sha256-L47JO1uVlCCh+JUQW6UCNDN/s2EtIGVZPd28t1sRNuA=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This add-on mod lets players engage with 1v1 battles with each other at any time!";
    homepage = "https://mods.sm64coopdx.com/mods/duels.9/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

