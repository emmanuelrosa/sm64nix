{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "widdle-pets";
  version = "1.1.1";

  src = fetchurl {
    url = "https://github.com/wibblus/widdle-pets/releases/download/v${version}/${pname}.zip";
    sha256 = "sha256-zN2pOqjMQA8iYAzCghjTMB4oOKL0Uvrh1RqVJRXbEsc=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "A buddy/pet mod for sm64coopdx, with API for other mods to add their own pets.";
    homepage = "https://github.com/wibblus/widdle-pets";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

