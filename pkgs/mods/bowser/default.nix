{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "bowser-character-select";
  version = "1.2";

  src = fetchurl {
    name = "bowser.zip";
    url = "https://mods.sm64coopdx.com/mods/bowser-jr-moveset.36/version/333/download";
    sha256 = "sha256-yAiqE2kgXCw7w0kp7E+M8Cyn6p6qgUp+m65/cyGISFQ=";
  };

  nativeBuildInputs = [ unzip ];
  
  outputs = [ "out" "doc" ];

  unpackCmd = ''
    mkdir ${pname}
    pushd ${pname}
    unzip $curSrc
    chmod -R ugo+r .
    popd
  '';

  installPhase = ''
    mkdir -p $out
    mkdir -p $doc

    cp -r . $out/

    rm -fR "$out/Bowser Moveset/.vs"
    mv $out/README.txt $doc/
    mv "$out/Template Character" $doc/
  '';

  meta = with lib; {
    description = "Bowser and Bowser Jr. character selection mod for SM64 Cooop Deluxe";
    longDescription = "Includes a shell spin move (crouch+B, moving), fireball (crouch+B), and various other tweaks and animations!";
    homepage = "https://mods.sm64coopdx.com/mods/bowser-jr-moveset.36/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

