{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-2";
  version = "2024-04-04";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-2-night-of-doom.80/version/86/download";
    sha256 = "sha256-xkGLK1LWsBWf3dfN/O0luyON2RWqsCjtoQVO0/Uqgj8=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "If you enjoy romhacks that abuse game mechanics such as quicksand, lava, fire spitters or chuckyas, then say no more!";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-2-night-of-doom.80/";
    longDescription = "This is the second romhack of the Star Revenge Series, now ported to sm64ex-coop. You really should not play this but it’ll be fun to see you try.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

