{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-7.5";
  version = "2024-07-08";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-7-5-kedowsers-return.221/version/354/download";
    sha256 = "sha256-3JC55w6IMnCmiNtE9Jk1mSmy1ppv3MSb0/e0Py4QAOE=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge 7.5, a SMG2 green stars styled hack based on Star Revenge 7.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-7-5-kedowsers-return.221/";
    longDescription = "After the failed attempt to stop Timerock, Kedowser was brought back and causes a lot of chaos in the Part and it's surroundings.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

