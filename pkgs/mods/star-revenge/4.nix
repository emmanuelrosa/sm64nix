{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-4";
  version = "2024-04-02";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-4-the-kedama-takeover-64.52/version/57/download";
    sha256 = "sha256-RYtHm2vc/RvSlDTCE7btBYHICOVkjLW9zFhwNyo4deU=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Star Revenge 4: The Kedama Takeover 64 is a ROM Hack made by BroDute and it’s the fourth entry in the Star Revenge series of hacks.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-4-the-kedama-takeover-64.52/";
    longDescription = "It has a total of 100 stars to collect in a big world of interconnected courses. Alongside many custom behaviors that aren’t seen often. And many unique gimmicks.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

