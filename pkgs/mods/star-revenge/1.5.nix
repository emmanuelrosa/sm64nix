{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-1.5";
  version = "2024-07-02";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://github.com/flipflopbell/SM64ExCoopMods/raw/989dbfffcd5fc7c6e61aa19a0027ad33fe93a33b/Star%20Revenge%201.5/Star%20Revenge%201.5.zip";
    sha256 = "sha256-lOn8NyB7eBoeDPdcClZ7a2A9Gz+M8nFvAoWfkQ8wSFU=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "A modern remake of Star Revenge 1 ST with 125 stars, which is also the 2nd time SR1 got remade.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-1-5-star-takeover-redone.226/";
    longDescription = "Mario finds himself at the edge of the Mushroom Kingdom, which has been overran by mysterious blue stars.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

