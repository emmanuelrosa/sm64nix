{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-3";
  version = "2024-07-09";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-3-mario-on-an-saoire.248/version/358/download";
    sha256 = "sha256-Cs+bAbyWYFASTdl9Mxw8lCfwpv3lH4oqgIgE+T6mw0Y=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This Star Revenge 3, Mario on a trip to a vacation island.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-3-mario-on-an-saoire.248/";
    longDescription = "After the events of Night of Doom, Mario and Peach set out on a vacation trip to An Saoire.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

