{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-0.5";
  version = "2024-04-07";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-0-5-unused-levels.98/version/106/download";
    sha256 = "sha256-oiyg7AP7yTq4a3iSsuZrdnTaMbOtmTMVk2EHdsI/Pro=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge 0.5.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-0-5-unused-levels.98/";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

