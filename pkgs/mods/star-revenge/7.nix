{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-7";
  version = "2024-07-10";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-7-park-of-time.144/version/366/download";
    sha256 = "sha256-aiW/5aOCVIjqMHGFP56sxKotDiCmMdC9PcWf+4o80ak=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Mario enters a strange park set up by Timerock. He losses some moves which get sealed into badges hidden across the park, which some new ones as well.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-7-park-of-time.144/";
    longDescription = "You enter a park, split into 3 sections based on water, the sky and various other environments. Upon entering Mario looses his ability to perform triple jumps and walljumps + his path is blocked by Blocks similar to stone and metal blocks from Paper Mario.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

