{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-5";
  version = "2024-07-09";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-5-neo-blue-realm.27/version/365/download";
    sha256 = "sha256-KfQLIURjiR+1eRAHSeKso+YQtHcU01hSEHrNdIcDEbw=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is a modern remake of Star Revenge 0.5 TUL with 70 stars, this is also one of the easiest SRs.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-5-neo-blue-realm.27/";
    longDescription = "After falling for a trap, Mario finds himself in the distant future, inside of the Blue Realm and tries to find his way out, back to his time.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

