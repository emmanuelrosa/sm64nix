{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-6";
  version = "2024-07-11";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-6-luigis-adventure.246/version/375/download";
    sha256 = "sha256-i35LdPS0dZ+7YD20idMf6nz5k8VPsAVr2Hdj3BcSeZY=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge 6, a take on exploring Peach's Castle that got taken over by Bowser.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-6-luigis-adventure.246/";
    longDescription = "After Mario went suddenly missing, Luigi sets out to stop Bowser.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

