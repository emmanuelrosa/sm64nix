{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-2.5";
  version = "2024-07-08";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-2-5-remnant-of-doom.239/version/353/download";
    sha256 = "sha256-TEo6+q1wjCKefy4dRmnxPlZ25y3A5b746E/pwUHcxuU=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge 2.5, a remake of Star Revenge 2 NoD";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-2-5-remnant-of-doom.239/";
    longDescription = "Bowser opens the seal of a forgotten temple and uses it's power to twist the world to his own liking.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

