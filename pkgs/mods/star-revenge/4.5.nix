{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-4.5";
  version = "2024-07-09";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-4-5-the-kedama-takeover-rewritten.75/version/364/download";
    sha256 = "sha256-zUkrLfLDc7eT0HH/UXtHYT/L802HG2QIqswKRYRTjzc=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Most of the levels are full re-imagines of the old models or new ones and course 10 and 11 are edited level models.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-4-5-the-kedama-takeover-rewritten.75/";
    longDescription = "While Mario had his trip to An Saoire, white creatures called Kedamas took over the Mushroom Kingdom and stole the stars of Peach's Castle and kidnapped the princess as well.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

