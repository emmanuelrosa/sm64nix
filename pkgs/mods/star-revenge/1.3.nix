{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-1.3";
  version = "2024-07-12";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-1-3-redone.249/version/374/download";
    sha256 = "sha256-WYOEeP55K8RmOdCKltBRWcBKbJCwj9/w7+IBZ/QeO5g=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge Redone or more commonly known as Redone 1.3, the old remake of the first Star Revenge.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-1-3-redone.249/";
    longDescription = "Funny stars mess with some part of Mushroom People Land, crazy stuff, do NOT play. This hack got replaced by SR1.5 Star Takeover Redone 2.0 in the timeline, you are better off to play that instead.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

