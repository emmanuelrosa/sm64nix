{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-1";
  version = "2024-04-04";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-1-star-takeover.81/version/87/download";
    sha256 = "sha256-j7PV5lbV+BVd6TLAnZbxkLgyicBbADAMwu28KbToFzM=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "Enjoy the first ever released hack of the Star Revenge Series, now in Cooperative!";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-1-star-takeover.81/";
    longDescription = "This romhack contains 101 stars and various classic levels that reappear in the redone version, and Weegee’s Challenge of course.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

