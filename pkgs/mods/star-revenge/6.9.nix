{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "star-revenge-6.9";
  version = "2024-07-08";

  src = fetchurl {
    name = "${pname}.zip";
    url = "https://mods.sm64coopdx.com/mods/star-revenge-6-9-luigi-lost-in-time.240/version/352/download";
    sha256 = "sha256-muPu9M02H9W/UsBWG2IeRPnKB8kPer3cuMVO+ExQYCY=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/${pname}
    cp -r . $out/${pname}/
  '';

  meta = with lib; {
    description = "This is Star Revenge 6.9, a small side-hack made from levels which were meant for SR8 before it got reworked to feature a lot more Touhou.";
    homepage = "https://mods.sm64coopdx.com/mods/star-revenge-6-9-luigi-lost-in-time.240/";
    longDescription = "You find yourself in a Tower with time portals to different points in the Star Revenge Timeline. From close to the beginning to the end, you find stars all across 3 main courses and a few smaller levels. Given the hint from the end of SR6.5, Luigi tries to confront Timerock in the tower he currently resides in.";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

