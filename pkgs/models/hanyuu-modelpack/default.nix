{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "hanyuu-modelpack";
  version = "2023-10-23";

  src = fetchurl {
    url = "https://filecache31.gamebanana.com/mods/hanyu_sm64.zip";
    sha256 = "sha256-NWxHKX+FT6DuwMWCQ15uge2Zpu7aEVr+ekCP2o1Xb1o=";
  };

  nativeBuildInputs = [ unzip ];

  unpackCmd = ''
    mkdir hanyuu
    unzip $curSrc -d hanyuu
  '';

  installPhase = ''
    mkdir -p $out/dynos/packs/Hanyuu
    cp dynos/mario_geo.bin "$out/dynos/packs/Hanyuu/"
  '';

  meta = with lib; {
    description = "Hanyuu (from the Higurashi series) model pack for Super Mario 64. Does not include sound pack, yet";
    homepage = "https://gamebanana.com/mods/475377";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

