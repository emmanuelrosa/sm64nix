{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "analogue-brothers-modelpack";
  version = "2024-03-03";

  src = fetchurl {
    name = "Analogue_Brothers.zip";
    url = "https://cdn.discordapp.com/attachments/716459185230970880/1214017064268861541/Analogue_Brothers.zip?ex=6689423f&is=6687f0bf&hm=1ea24b00410ddf1de6b71655e7a1e01ec5d198abc665cc906328ea67beba6829&";
    sha256 = "sha256-D3+5RYlVMtCVdCxdo4PROkrAfhyA0HFml2uQw8YyUA4=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p "$out/dynos/packs/AnalogueBrothers"
    cp -r . "$out/dynos/packs/AnalogueBrothers/"
    rm -fR "$out/dynos/packs/AnalogueBrothers/luigi"
    rm -fR "$out/dynos/packs/AnalogueBrothers/mario"
  '';

  meta = with lib; {
    description = "Mario 64 Classified's model pack based on the final episode, Epilogue, for Render96. Includes both Mario and Luigi.";
    homepage = "https://discord.com/channels/707763437975109784/716459185230970880/1214017064692359228";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

