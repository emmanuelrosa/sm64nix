{ lib
, stdenv
, fetchurl
, p7zip
}: stdenv.mkDerivation rec {
  pname = "render96-modelpack";
  version = "3.2";

  src = fetchurl {
    url = "https://github.com/Render96/ModelPack/releases/download/${version}/Render96_DynOs_v${version}.7z";
    sha256 = "sha256-2V7yWmlLHNLoJ4VBiuvyqo/TprOUs1D+UZcUdNGWZLI=";
  };

  nativeBuildInputs = [ p7zip ];

  unpackCmd = ''
    7z x $curSrc
  '';

  installPhase = ''
    mkdir -p "$out/dynos/packs/Render96"
    mkdir -p "$out/dynos/packs/Render96 Mario"
    mkdir -p "$out/dynos/packs/Render96 Luigi"
    mkdir -p "$out/dynos/packs/Render96 Wario"
    cp -r . "$out/dynos/packs/Render96/"

    cp "$out/dynos/packs/Render96/mario_geo.bin" "$out/dynos/packs/Render96 Mario/"
    cp "$out/dynos/packs/Render96/marios_cap_geo.bin" "$out/dynos/packs/Render96 Mario/"
    cp "$out/dynos/packs/Render96/marios_metal_cap_geo.bin" "$out/dynos/packs/Render96 Mario/"
    cp "$out/dynos/packs/Render96/marios_winged_metal_cap_geo.bin" "$out/dynos/packs/Render96 Mario/"
    cp "$out/dynos/packs/Render96/marios_wing_cap_geo.bin" "$out/dynos/packs/Render96 Mario/"

    cp "$out/dynos/packs/Render96/luigi_geo.bin" "$out/dynos/packs/Render96 Luigi/mario_geo.bin"
    cp "$out/dynos/packs/Render96/luigis_cap_geo.bin" "$out/dynos/packs/Render96 Luigi/marios_cap_geo.bin"
    cp "$out/dynos/packs/Render96/luigis_metal_cap_geo.bin" "$out/dynos/packs/Render96 Luigi/marios_metal_cap_geo.bin"
    cp "$out/dynos/packs/Render96/luigis_winged_metal_cap_geo.bin" "$out/dynos/packs/Render96 Luigi/marios_winged_metal_cap_geo.bin"
    cp "$out/dynos/packs/Render96/luigis_wing_cap_geo.bin" "$out/dynos/packs/Render96 Luigi/marios_wing_cap_geo.bin"

    cp "$out/dynos/packs/Render96/wario_geo.bin" "$out/dynos/packs/Render96 Wario/mario_geo.bin"
    cp "$out/dynos/packs/Render96/warios_cap_geo.bin" "$out/dynos/packs/Render96 Wario/marios_cap_geo.bin"
    cp "$out/dynos/packs/Render96/warios_metal_cap_geo.bin" "$out/dynos/packs/Render96 Wario/marios_metal_cap_geo.bin"
    cp "$out/dynos/packs/Render96/warios_winged_metal_cap_geo.bin" "$out/dynos/packs/Render96 Wario/marios_winged_metal_cap_geo.bin"
    cp "$out/dynos/packs/Render96/warios_wing_cap_geo.bin" "$out/dynos/packs/Render96 Wario/marios_wing_cap_geo.bin"
  '';

  meta = with lib; {
    description = "Render96 model pack for Super Mario 64.";
    longDescription = "This model pack is divided into multiple DynOS so that you can easily choose from various characters. However, the voice is always Mario's. Admittedly, this is a hacky form of character selection.";
    homepage = "https://github.com/Render96/ModelPack";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

