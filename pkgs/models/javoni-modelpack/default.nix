{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "javoni-modelpack";
  version = "d35fc";

  src = fetchurl {
    url = "https://filecache33.gamebanana.com/mods/javoni_${version}.zip";
    sha256 = "sha256-MsxSaKg1hBY3Juvyx8YrOw2PaCg07+eKhiO6AesHe4w=";
  };

  nativeBuildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/dynos/packs/Javoni
    cp mario_geo.bin "$out/dynos/packs/Javoni/"
  '';

  meta = with lib; {
    description = "Javoni model pack for Super Mario 64.";
    homepage = "https://gamebanana.com/mods/518096";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

