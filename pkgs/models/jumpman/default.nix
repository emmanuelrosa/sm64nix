{ lib
, stdenv
, fetchurl
, unzip
}: stdenv.mkDerivation rec {
  pname = "jumpman-modelpack";
  version = "2024-03-05";

  src = fetchurl {
    name = "Tillys_Jumpman_Pack.zip";
    url = "https://cdn.discordapp.com/attachments/716459185230970880/1214646632633602088/Tillys_Jumpman_Pack.zip?ex=6688e994&is=66879814&hm=3269cfd7d28853b70a7738824c2d9ba798f333090e9cc0b3a59cbf4c42f21848&";
    sha256 = "sha256-70LaVqBnLZE22A+nSUIETENwj3JKShiQxv5Xfqx2uaQ=";
  };

  nativeBuildInputs = [ unzip ];

  unpackCmd = ''
    mkdir ${pname}
    pushd ${pname}
    unzip $curSrc
    chmod -R ugo+r .
    popd
  '';

  installPhase = ''
    mkdir -p "$out/dynos/packs/High-Poly-Jumpman"
    mkdir -p "$out/dynos/packs/Low-Poly-Jumpman"
    cp -r ./Jumpman/. "$out/dynos/packs/High-Poly-Jumpman/"
    cp -r ./LowPolyJumpman/. "$out/dynos/packs/Low-Poly-Jumpman/"
  '';

  meta = with lib; {
    description = "Jumpman model pack based for Render96. Includes high and low-poly models.";
    homepage = "https://discord.com/channels/707763437975109784/716459185230970880/1214646633279651850";
    license = licenses.free;
    platforms = platforms.linux;
    maintainers = with maintainers; [ emmanuelrosa ];
  };
}

